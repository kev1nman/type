<section class="contacto">
    <div class="row">
        <div class="col-md-5 box contactoFondo">
            <div class="triangule">

            </div>
        </div>
        <div class="col-md-7 box contactoBox">
            <div class="titulo">
                <i class="ion-email"></i>
                <h1>
                    Contáctanos
                </h1>
                <h2>
                    Agenda una cita con nosotros, cuéntanos más en qué te podemos ayudar
                </h2>
                <div class="lineaBottom"></div>
            </div>
            <form action="">
                <div class="form-group">
                    <input type="text" required="required"/>
                    <label class="control-label" for="input">Nombre</label><i class="bar"></i>
                </div>
                <div class="form-group">
                    <input type="number" required="required"/>
                    <label class="control-label" for="input">Teléfono</label><i class="bar"></i>
                </div>
                <div class="form-group">
                    <input type="email" required="required"/>
                    <label class="control-label" for="input">Email</label><i class="bar"></i>
                </div>
                <div class="form-group">
                    <textarea required="required"></textarea>
                    <label class="control-label" for="textarea">Mensaje</label><i class="bar"></i>
                </div>
                <div class="button-container">
                    <button class="buttonMaterial" type="button"><span>Enviar</span></button>
                </div>
            </form>
        </div>
    </div>

</section>