<footer>
    <section class="links">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1>Productos y servicios</h1>
                    <ul>
                        <li>Consultoría y asesoria</li>
                        <li>Desarrollo de software</li>
                        <li>App móviles</li>
                        <li>Marketing digital</li>
                        <li>Business process managment</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h1>Soluciones</h1>
                    <ul>
                        <li>T-CMS</li>
                        <li>Email marketing</li>
                        <li>T-COTIZADOR</li>
                        <li>T-CRM</li>
                        <li>T-FORMS</li>
                        <li>T-SURVEY</li>
                    </ul>
                </div>
                <div class="col-md-4 redes">
                    <ul>
                        <li><i class="ion-social-whatsapp-outline"></i>+56 5432 32 34</li>
                        <li><i class="ion-social-facebook-outline"></i>Type.cl</li>
                        <li><i class="ion-social-twitter-outline"></i>Type.cl</li>
                        <li><i class="ion-social-instagram-outline"></i>Type.cl</li>
                    </ul>
                    <h2>377 Noemi Mount Suite 799</h2>
                    <h3>Santiago, Chile.</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="footer center">
        The Almighty Beer Can Chicken
    </section>
</footer>