<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Type.cl</title>
	<meta name="description" content="Blueprint: A basic template for a page stack navigation effect" />
	<meta name="keywords" content="blueprint, template, html, css, page stack, 3d, perspective, navigation, menu" />
	<meta name="author" content="Codrops" />
	<link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'>
	<link rel='icon' href='img/favicon.ico' type='image/x-icon'>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/feather/style.css">
	<link rel="stylesheet" type="text/css" href="css/componentZoom.css" />
	<link rel="stylesheet" type="text/css" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
	<script src="js/modernizr-custom.js"></script>
	<script src="js/modernizrZoom.custom.js"></script>

	<link rel="stylesheet" type="text/css" href="css/galeria.css" />
	<link rel="stylesheet" type="text/css" href="css/formMaterial.css" />
	<link rel="stylesheet" type="text/css" href="css/my-app.css" />


	
</head>

<body>