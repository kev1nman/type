<!-- navigation -->
<nav class="pages-nav">
<div class="pages-nav__item"><a class="link link--page2" href="./">Home</a></div>
<div class="pages-nav__item"><a class="link link--page" href="#page-t-cms">Consultoria y asesoría</a></div>
<div class="pages-nav__item"><a class="link link--page" href="#page-t-cms">Desarrollo de software</a></div>
<div class="pages-nav__item"><a class="link link--page" href="#page-t-cms">App móviles</a></div>
<div class="pages-nav__item"><a class="link link--page" href="#page-t-cms">Marketing digital</a></div>
<div class="pages-nav__item"><a class="link link--page" href="#page-t-cms">Business process managment</a></div>
<div class="pages-nav__item pages-nav__item--small"><a class="link link--page link--faded" href="#page-contact">Contácto</a></div>
<div class="pages-nav__item pages-nav__item--social">
    <a class="link link--social link--faded" href="#"><i class="fa fa-twitter"></i><span class="text-hidden">Twitter</span></a>
    <a class="link link--social link--faded" href="#"><i class="fa fa-linkedin"></i><span class="text-hidden">LinkedIn</span></a>
    <a class="link link--social link--faded" href="#"><i class="fa fa-facebook"></i><span class="text-hidden">Facebook</span></a>
    <a class="link link--social link--faded" href="#"><i class="fa fa-youtube-play"></i><span class="text-hidden">YouTube</span></a>
</div>
</nav>
<!-- /navigation-->