<div class="page" id="page-productos">
<div class="container-fluid">


    <div class="interno">
        <!-- page -->

        <section class="headerBackground2">
            <!-- Blueprint header -->
            <header class="bp-header cf">
                
            </header>
        </section>


        <section class="interno2 internoGeneral">
            <div class="container">
                <h1>Productos y servicios</h1>
                <div class="row">
                    <div class="col-md-4">
                        <h2><span class="ion-cube"></span></h2>
                        <h2>
                            Ayudámos a formalizar y/o descubrir sus procesos de negocio                            
                        </h2>
                    </div>
                    <div class="col-md-4">
                        <h2><span class="ion-cube"></span></h2>
                        <h2>
                            Potenciamos sus herramientas tecnológicas para fortalecer sus áreas de negocio
                        </h2>
                    </div>
                    <div class="col-md-4">
                        <h2><span class="ion-cube"></span></h2>
                        <h2>
                            Identificamos la mejor Estrategia Digital para sus objetivos utilizando las redes Ad hoc a su mercado
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">Consultoria y asesoría</a></li>
                                <li role="presentation"><a href="#2" aria-controls="2" role="tab" data-toggle="tab">Desarrollo de software</a></li>
                                <li role="presentation"><a href="#3" aria-controls="3" role="tab" data-toggle="tab">App móviles</a></li>
                                <li role="presentation"><a href="#4" aria-controls="4" role="tab" data-toggle="tab">Marketing digital</a></li>
                                <li role="presentation"><a href="#5" aria-controls="5" role="tab" data-toggle="tab">Business process managment</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="1">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <div class="imgSol imgProduct">
                                                <img src="img/1.png" alt="">
                                            </div>
                                            <h1>Consultoria y asesoría</h1>
                                            <p class="text-center">
                                                Tus clientes necesitan mantenerse informados sobre tu Empresa, Servicios y Productos. <span>Informales!..</span>
                                            </p>
                                            <div class="button-container">
                                                <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="img"></div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="2">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <div class="imgSol imgProduct">
                                                <img src="img/2.png" alt="">
                                            </div>
                                            <h1>Desarrollo de software</h1>
                                            <p class="text-center">
                                                Tus clientes necesitan mantenerse informados sobre tu Empresa, Servicios y Productos. <span>Informales!..</span>
                                            </p>
                                            <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="img"></div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="3">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <div class="imgSol imgProduct">
                                                <img src="img/3.png" alt="">
                                            </div>
                                            <h1>App móviles</h1>
                                            <p class="text-center">
                                                No pierdas las oportunidades de negocio que se generan cuando tus clientes visitan tu Web.<span>Captalos!</span>
                                            </p>
                                            <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="img"></div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="4">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <div class="imgSol imgProduct">
                                                <img src="img/4.png" alt="">
                                            </div>
                                            <h1>Marketing digital</h1>
                                            <p class="text-center">
                                                Centraliza toda la información de tus clientes en un mismo lugar y ten el control de la gestión sobre ellos al detalle.<span>Atrevete!</span>
                                            </p>
                                            <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="img"></div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="5">
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <div class="imgSol imgProduct">
                                                <img src="img/5.png" alt="">
                                            </div>
                                            <h1>Business process managment</h1>
                                            <p class="text-center">
                                                La Web es inmensa entonces ¿Porqué rescatar información de tus clientes solo en tu web?.<span>Muestrate!</span>
                                            </p>
                                            <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="img"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


        
    </div>


    <?php include "partials/contacto.php"; include "partials/footer.php"; ?>
</div>
</div>