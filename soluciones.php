<div class="page" id="page-soluciones">
    <div class="container-fluid">


        <div class="interno">
            <!-- page -->

            <section class="headerBackground2">
                <!-- Blueprint header -->
                <header class="bp-header cf">
                    
                </header>
            </section>


            <section class="interno2 internoGeneral">
                <div class="container">
                    <h1>Soluciones on Demand</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabs">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#t-cms" aria-controls="t-cms" role="tab" data-toggle="tab">T-CMS</a></li>
                                    <li role="presentation"><a href="#email" aria-controls="email" role="tab" data-toggle="tab">EMAIl MARKETING</a></li>
                                    <li role="presentation"><a href="#t-contizador" aria-controls="t-contizador" role="tab" data-toggle="tab">T-COTIZADOR</a></li>
                                    <li role="presentation"><a href="#t-crm" aria-controls="t-crm" role="tab" data-toggle="tab">T-CRM</a></li>
                                    <li role="presentation"><a href="#t-forms" aria-controls="t-forms" role="tab" data-toggle="tab">T-FORMS</a></li>
                                    <li role="presentation"><a href="#t-survey" aria-controls="t-survey" role="tab" data-toggle="tab">T-SURVEY</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="t-cms">
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <div class="imgSol">
                                                    <img src="img/sol1.png" alt="">
                                                </div>
                                                <h1>T-CMS</h1>
                                                <p class="text-center">
                                                    Tus clientes necesitan mantenerse informados sobre tu Empresa, Servicios y Productos. <span>Informales!..</span>
                                                </p>
                                                <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="img"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="email">
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <div class="imgSol">
                                                    <img src="img/sol2.png" alt="">
                                                </div>
                                                <h1>EMAIl MARKETING</h1>
                                                <p class="text-center">
                                                    Tus clientes necesitan mantenerse informados sobre tu Empresa, Servicios y Productos. <span>Informales!..</span>
                                                </p>
                                                <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="img"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="t-contizador">
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <div class="imgSol">
                                                    <img src="img/sol3.png" alt="">
                                                </div>
                                                <h1>T-COTIZADOR</h1>
                                                <p class="text-center">
                                                    No pierdas las oportunidades de negocio que se generan cuando tus clientes visitan tu Web.<span>Captalos!</span>
                                                </p>
                                                <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="img"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="t-crm">
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <div class="imgSol">
                                                    <img src="img/sol4.png" alt="">
                                                </div>
                                                <h1>T-CRM</h1>
                                                <p class="text-center">
                                                    Centraliza toda la información de tus clientes en un mismo lugar y ten el control de la gestión sobre ellos al detalle.<span>Atrevete!</span>
                                                </p>
                                                <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="img"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="t-forms">
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <div class="imgSol">
                                                    <img src="img/sol5.png" alt="">
                                                </div>
                                                <h1>T-FORMS</h1>
                                                <p class="text-center">
                                                    La Web es inmensa entonces ¿Porqué rescatar información de tus clientes solo en tu web?.<span>Muestrate!</span>
                                                </p>
                                                <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="img"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="t-survey">
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <div class="imgSol">
                                                    <img src="img/sol6.png" alt="">
                                                </div>
                                                <h1>T-SURVEY</h1>
                                                <p class="text-center">
                                                    Genera una conversación activa con tus clientes, conoce sus necesidades, su opinión por tus servicios y productos.<span>Escuchalos!</span>
                                                </p>
                                                <div class="button-container">
                                                    <button class="buttonMaterial" type="button"><span>Leer mas</span></button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="img"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>


            
        </div>


        <?php include "partials/contacto.php"; include "partials/footer.php"; ?>
    </div>
</div>