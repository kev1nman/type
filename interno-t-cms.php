<div class="page" id="page-t-cms">
    <div class="container-fluid">


        <div class="interno">
            <!-- page -->

            <section class="headerBackground2">
                <!-- Blueprint header -->
                <header class="bp-header cf">

                </header>
            </section>



            <section class="sect1 interno1">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 center">
                            <h1>
                                Type - Custom Managment System
                            </h1>
                        </div>
                    </div>
                </div>
            </section>

            <section class="interno2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li><a href="./">Home</a></li>
                                <li>Soluciones</li>
                                <li class="active">T-CMS</li>
                            </ol>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                <img src="./img/sol1.png" alt="">
                                <h1>T-CMS</h1>
                            </div>
                            <p>
                                Permite configurar de forma simple y rápida su sitio web secciones:
                            </p>
                            <ul>
                                <li>Home Dinámico</li>
                                <li>Sobre Empresa</li>
                                <li>Ficha de productos y/o servicios</li>
                                <li>Noticias</li>
                                <li>Formulario de Contacto</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <div id="quad">
                                <figure>
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/rose-red-wine.jpg" alt="rose-red-wine">
                                <figcaption>Rose Red Wine</figcaption>
                                </figure>
                                <figure>
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/green-glass-bottle.jpg" alt>
                                    <figcaption>Green Glass Bottle</figcaption>
                                </figure>
                                <figure>
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/guinness-barrels.jpg" alt>
                                    <figcaption>Guinness Barrels, Dublin</figcaption>
                                </figure>
                                <figure>
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/crystal-skull-vodka.jpg" alt>
                                    <figcaption>Crystal Skull Vodka</figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="button-container">
                                <button class="buttonMaterial" type="button"><span>Me interesa</span></button>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-12">
                        <div class="tabs">
                        
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#integracion" aria-controls="integracion" role="tab" data-toggle="tab">Integración</a></li>
                            <li role="presentation"><a href="#modalidad" aria-controls="modalidad" role="tab" data-toggle="tab">Modalidad</a></li>
                          </ul>
                        
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="integracion">
                                <div class="">
                                    <div class="col-md-6">
                                        <h3>
                                            T- CMS Integrado con su dominio de negocio
                                        </h3>
                                        <p>
                                            Esta característica permite asociar 1 o más dominios a T-CSM, en los cuales pueden configurar el FrontEnd de su sitio web.
                                        </p>
                                        <div class="cuadros">
                                            <div class="cuadro1">
                                                <h1>
                                                    Backend
                                                </h1>
                                                <p>
                                                    T- CMS <br>
                                                    (Administrador de contenido)
                                                </p>
                                            </div>
                                            <span class="ion-ios-arrow-forward"></span>
                                            <div class="cuadro2">
                                                <h1>
                                                    Frontend
                                                </h1>
                                                <p>
                                                    T- CMS <br>
                                                    (Administrador de contenido)
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 img">
                            
                                    </div>
                                </div>
                                
                            </div>
                            <div role="tabpanel" class="tab-pane" id="modalidad">2</div>
                          </div>
                        
                        </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="sect4 interno3">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="white titulo">
                                <i class="ion-earth"></i>
                                <h1>
                                    Otras soluciones
                                </h1>
                                <div class="lineaBottom"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-1 box solucion">
                            <div class="img">
                                <img src="img/sol2.png" alt="">
                            </div>
                            <h1>EMAIl MARKETING</h1>
                        </div>
                        <div class="col-sm-2 box solucion">
                            <div class="img">
                                <img src="img/sol3.png" alt="">
                            </div>
                            <h1>T-COTIZADOR</h1>
                        </div>
                        <div class="col-sm-2 box solucion">
                            <div class="img">
                                <img src="img/sol4.png" alt="">
                            </div>
                            <h1>T-CRM</h1>
                        </div>
                        <div class="col-sm-2 box solucion">
                            <div class="img">
                                <img src="img/sol5.png" alt="">
                            </div>
                            <h1>T-FORMS</h1>
                        </div>
                        <div class="col-sm-2 box solucion">
                            <div class="img">
                                <img src="img/sol6.png" alt="">
                            </div>
                            <h1>T-SURVEY</h1>
                        </div>
                    </div>
                </div>
            </section>



        </div>

        <?php include "partials/contacto.php"; include "partials/footer.php"; ?>


    </div>


</div>
<script src="js/galeria.js"></script>