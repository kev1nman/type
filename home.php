<div class="page" id="page-home">
			<div class="container-fluid">
				<!-- Grid -->
				<section class="slider">
					<div class="slide slide--current" data-content="content-3">
						<div class="slide__mover">
							<div class="zoomer flex-center">
								<img class="zoomer__image" src="images/macbook.png" alt="MacBook" />
								<div class="preview">
									<img src="images/macbook-content-preview.jpg" alt="MacBook app preview" />
									<div class="zoomer__area zoomer__area--size-3"></div>
								</div>
							</div>
						</div>
						<h2 class="slide__title"><span>Customer Relationship Management</span> CRM</h2>
					</div>
					<div class="slide" data-content="content-1">
						<div class="slide__mover">
							<div class="zoomer flex-center">
								<img class="zoomer__image" src="images/iphone.png" alt="iPhone" />
								<div class="preview">
									<img src="images/iphone-content-preview.png" alt="iPhone app preview" />
									<div class="zoomer__area zoomer__area--size-2"></div>
								</div>
							</div>
						</div>
						<h2 class="slide__title"><span>The Classy</span> iPhone 6</h2>
					</div>
					<div class="slide" data-content="content-4">
						<div class="slide__mover">
							<div class="zoomer flex-center">
								<img class="zoomer__image" src="images/imac.png" alt="iMac" />
								<div class="preview">
									<img src="images/imac-content-preview.jpg" alt="iMac app preview" />
									<div class="zoomer__area zoomer__area--size-5"></div>
								</div>
							</div>
						</div>
						<h2 class="slide__title"><span>The Glorious</span> iMac</h2>
					</div>
					<nav class="slider__nav">
						<button class="button button--nav-prev"><i class="icon icon--arrow-left"></i><span class="text-hidden">Previous product</span></button>
						<button id="vermas" class="button button--zoom"><i class="icon icon--zoom"></i><span class="text-hidden">View details</span> Ver mas</button>
						<button class="button button--nav-next" id="next"><i class="icon icon--arrow-right"></i><span class="text-hidden">Next product</span></button>
					</nav>
				</section>
				<!-- /slider-->
				<section class="content">
					<div class="content__item" id="content-3">
						<img class="content__item-img rounded-right" src="images/macbook-content.jpg" alt="MacBook Content" />
						<div class="content__item-inner">
							<h2>CRM</h2>
							<h3>Customer Relationship Management</h3>
							<p>With the new MacBook, we set out to do the impossible: engineer a full-size experience into the lightest and most compact Mac notebook ever. That meant reimagining every element to make it not only lighter and thinner but also better. The result is more than just a new notebook. It's the future of the notebook.</p>
							<p><a class="btn btn-primary" href="#">Quiero saber más</a></p>
						</div>
					</div>
					<div class="content__item" id="content-1">
						<img class="content__item-img rounded-right" src="images/iphone-content.jpg" alt="Apple Watch Content" />
						<div class="content__item-inner">
							<h2>The iPhone 6</h2>
							<h3>Incredible performance for powerful apps</h3>
							<p>Built on 64-bit desktop-class architecture, the new A8 chip delivers more power, even while driving a larger display. The M8 motion coprocessor efficiently gathers data from advanced sensors and a new barometer. And with increased battery life, iPhone 6 lets you do more, for longer than ever.</p>
							<p><a class="btn btn-primary" href="#">Quiero saber más</a></p>
						</div>
					</div>
					<div class="content__item" id="content-4">
						<img class="content__item-img rounded-right" src="images/imac-content.jpg" alt="iMac Content" />
						<div class="content__item-inner">
							<h2>The iMac</h2>
							<h3>Engineered to the very last detail</h3>
							<p>Every new Mac comes with Photos, iMovie, GarageBand, Pages, Numbers, and Keynote. So you can be creative with your photos, videos, music, documents, spreadsheets, and presentations right from the start. You also get great apps for email, surfing the web, sending texts, and making FaceTime calls — there’s even an app for finding new apps.</p>
							<p><a class="btn btn-primary" href="#">Quiero saber más</a></p>
						</div>
					</div>
					<button id="cerrar" class="button button--close"><i class="icon icon--circle-cross"></i><span class="text-hidden">Close content</span></button>
				</section>
	
				
				<section class="sect1">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 center">
								<h2>
									Un socio estratégico en soluciones tecnológicas
								</h2>
							</div>
						</div>
					</div>
				</section>

				<section class="sect2">
					<div class="container-fuild">
						<div class="row">
							<div class="col-xs-12 center noP">
								<div class="box titulo servicioTitulo">
									<i class="ion-cube"></i>
									<h1>
										Productos y servicios
									</h1>
									<div class="lineaBottom"></div>
								</div>
							</div>
						</div>
						<div class="row">
							<a href="./internoProducts.php">
								<div class="col-md-3 box servicio servicio1">
									<div class="">
										<img src="img/1.png"  alt="">
										<h2>Consultoria y asesoría</h2>
									</div>
								</div>
							</a>
							<a href="./internoProducts.php">
								<div class="col-md-6 box servicio servicio2">
									<div class="">
										<img src="img/2.png"  alt="">
										<h2>Desarrollo de software</h2>
									</div>
								</div>
							</a>
							<a href="./internoProducts.php">
								<div class="col-md-3 box servicio servicio3">
									<div class="">
										<img src="img/3.png"  alt="">
										<h2>App móviles</h2>
									</div>
								</div>
							</a>
						</div>
						<div class="row">
							<a href="./internoProducts.php">
								<div class="col-md-6 box servicio servicio4">
									<div class="">
										<img src="img/4.png"  alt="">
										<h2>Marketing digital</h2>
									</div>
								</div>
							</a>
							<a href="./internoProducts.php">
								<div class="col-md-6 box servicio servicio5">
									<div class="">
										<img src="img/5.png"  alt="">
										<h2>Business process managment</h2>
									</div>
								</div>
							</a>
						</div>
					</div>
				</section>


				<section class="sect4">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="box titulo">
									<i class="ion-earth"></i>
									<h1>
										Soluciones on demand
									</h1>
									<div class="lineaBottom"></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4 box solucion">
							<a href="./internoSol.php">
								<div class="img">
									<img src="img/sol1.png" alt="">
								</div>
								<h1>T-CMS</h1>
							</a>
							<p class="text-center">
								Tus clientes necesitan mantenerse informados sobre tu Empresa, Servicios y Productos. <span>Informales!..</span>
							</p>
							
							</div>
							<div class="col-sm-4 box solucion">
							<a href="./internoSol.php">
								<div class="img">
									<img src="img/sol2.png" alt="">
								</div>
								<h1>EMAIl MARKETING</h1
							</a>>
								<p class="text-center">
									Tus clientes necesitan mantenerse informados sobre tu Empresa, Servicios y Productos. <span>Informales!..</span>
								</p>
							</div>
							<div class="col-sm-4 box solucion">
							<a href="./internoSol.php">
								<div class="img">
									<img src="img/sol3.png" alt="">
								</div>
								<h1>T-COTIZADOR</h1>
							</a>
								<p class="text-center">
									No pierdas las oportunidades de negocio que se generan cuando tus clientes visitan tu Web. 
									<span>Captalos!</span>
																	</p>
							</div>
							<div class="col-sm-4 box solucion">
							<a href="./internoSol.php">
								<div class="img">
									<img src="img/sol4.png" alt="">
								</div>
								<h1>T-CRM</h1>
							</a>
								<p class="text-center">
									Centraliza toda la información de tus clientes en un mismo lugar y ten el control de la gestión sobre ellos al detalle.
									<span>Atrevete!</span>
																	</p>
							</div>
							<div class="col-sm-4 box solucion">
							<a href="./internoSol.php">
								<div class="img">
									<img src="img/sol5.png" alt="">
								</div>
								<h1>T-FORMS</h1>
							</a>
								<p class="text-center">
									La Web es inmensa entonces ¿Porqué rescatar información de tus clientes solo en tu web?.
									<span>Muestrate!</span>
																	</p>
							</div>
							<div class="col-sm-4 box solucion">
							<a href="./internoSol.php">
								<div class="img">
									<img src="img/sol6.png" alt="">
								</div>
								<h1>T-SURVEY</h1>
							</a>
								<p class="text-center">
									Genera una conversación activa con tus clientes, conoce sus necesidades, su opinión por tus servicios y productos.
									<span>Escuchalos!</span>
																	</p>
							</div>
						</div>
					</div>
				</section>


				<section class="sect3">
					<div class="row">
						<div class="col-md-6 box quienesSomos">
							<div class="triangule">

							</div>
						</div>
						<div class="col-md-6 box boxNuestraMision">
							<div class="titulo white">
								<i class="ion-ios-people"></i>
								<h1>
									Quiénes somos
								</h1>
								<div class="lineaBottom"></div>
							</div>
							<p>
								Una consultora enfocada a la generación de soluciones tecnológicas compuesta por profesionales formales, creativos y apasionados por la innovación.
								<br><br>
								Nuestra motivación y desafío constante es crear y perfeccionar soluciones de alto nivel que permitan a nuestros clientes desarrollar todo su potencial para alcanzar sus objetivos propuestos.
							</p>
							
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 box">
							<div class="titulo">
								<i class="ion-android-happy"></i>
								<h1>
									Nuestra motivación
								</h1>
								<div class="lineaBottom"></div>
							</div>
							<p>
								Ser un referente que esté a la vanguardia del conocimiento tanto de negocio y tecnologías 
								para satisfacer las necesidades de los clientes que confíen en nuestros servicios. 
								<br><br>
								Lograr el reconocimiento de nuestro trabajo y hacer de ellos un caso de éxito para la sociedad.
							</p>
						</div>
						<div class="col-md-6 box nuestraMision">
							<div class="triangule">
								
							</div>
						</div>
					</div>
				</section>


				<section class="sect1 negro">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 center">
								<div class="titulo white">
									<i class="ion-android-document"></i>
									<h1>
										Modelo de negocios
									</h1>
									<div class="lineaBottom"></div>
								</div>
							</div>
						</div>
					</div>
				</section>




				<section class="sect5">
					<div class="row">
						<div class="col-md-6 box mod1">
							<div class="titulo">
								<h1>1</h1>
								<h1>
									Lanzamiento de procesos
								</h1>
								<h2>Consultoria</h2>
							</div>
							<ul>
								<li>Revisión del estado actual</li>
								<li>Entrevista - Observaciones</li>
								<li>Documentación</li>
							</ul>
						</div>
						<div class="col-md-6 box mod2">
							<div class="triangule">
									
							</div>
							<div class="titulo">
								<h1>2</h1>
								<h1>
									Análisis de Mejoras
								</h1>
								<h2>Asesoría</h2>
							</div>
							<ul>
								<li>Análisis de problema</li>
								<li>Análisis de causas</li>
								<li>Evaluación de alternativas</li>
								<li>Modelamiento de Mejoras</li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 box mod3">
							<div class="triangule">
									
							</div>
							<div class="titulo">
								<h1>4</h1>
								<h1>
									Análisis de Mejoras
								</h1>
								<h2>Mejora Continua</h2>
							</div>
							<ul>
								<li>Supervición de cumplimiento de objetivos</li>
							</ul>
						</div>
						<div class="col-md-6 box mod4">
							<div class="triangule">
								
							</div>
							<div class="titulo">
								<h1>3</h1>
								<h1>
									Monitoreo de Solución
								</h1>
								<h2>Desarrollo e Innovación</h2>
							</div>
							<ul>
								<li>Desarrollo de técnicas </li>
								<li>Desarrollo de Soluciones Tecnológicas</li>
								<li>Control de Cambios</li>
								<li>Automatización de Procesos o Tareas</li>
							</ul>
						</div>
					</div>
				</section>



				<section class="sect6">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 center">
								<img src="img/user.jpg" alt="">
								<div class="row">
									<div class="col-xs-1 center">
										<i class="ion-quote left"></i>
									</div>
									<div class="col-xs-10">
										<p>
											The Sedu hair straightening method is done with a Sedu straightening iron, which has ceramic plates an inch to an inch and a half wide. There are thicker plates to be used with longer-length hair and there are thinner ceramic plates to use with shorter hair. 
										</p>
									</div>
									<div class="col-xs-1 center">
										<i class="ion-quote right"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>


				


				<?php include "partials/contacto.php"; include "partials/footer.php"; ?>
				

			</div>
				
			
		</div>